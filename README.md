# Solución al ejercicio del cliente de chat

Esta solución es la implementación mínima para el ejercicio planteado en clase.

Incluye estilos y todo el funcionamiento básico, pero tiene muchas mejoras pendientes para el futuro, incluyendo:

- Recargar mensajes periódicamente y/o al enviar uno nuevo
- Paginar historial de chat
- Editar imagen de usuario
- ....
