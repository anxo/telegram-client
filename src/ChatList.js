import ChatEntry from './ChatEntry'
import useFetch from './useFetch'
import './ChatList.css'
import { useSelector } from 'react-redux'

function ChatList() {
  const me = useSelector(s => s.user)
  useFetch('http://telegram-api.trek-quest.com/chats', 'fetch/chatlist')
  const contactObj = useSelector(s => s.contacts)
  const messagesObj = useSelector(s => s.messages)
  const chats = Object.values(contactObj).map(c => ({
    ...c,
    lastMessage: (messagesObj[c.id + ""] || [])[0]
  }))

  return (
    <aside className="chat-list">
      <div className="my-info">
        <div className="avatar" style={{ backgroundImage: `url(${me.avatar})`}} />
        <div className="actions">
          ...
        </div>
      </div>
      <div className="entries">
        {chats?.map(chat =>
          <ChatEntry key={chat.id} chat={chat} />
        )}
      </div>
    </aside>
  )
}

export default ChatList
