import { useParams } from 'react-router-dom'
import './ChatRoom.css'
import { useState } from 'react'
import useChat from './useChat'
import { useSelector } from 'react-redux'
import useFetch from './useFetch'

function ChatRoom() {
  const { id } = useParams()
  useFetch('http://telegram-api.trek-quest.com/chats/' + id, 'fetch/chat')
  useChat()
  const user = useSelector(s => s.user)
  const [message, setMessage] = useState('')

  const handleSubmit = e => {
    e.preventDefault()
    setMessage('')
    fetch('http://telegram-api.trek-quest.com/chats/' + id, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + user.token
      },
      body: JSON.stringify({ message })
    })
  }

  const contact = useSelector(s => s.contacts[id])
  const messages = useSelector(s => s.messages[id]) || []

  return (
    <div className="dbc chat-room">
      {contact &&
        <header>
          <div className="avatar" style={{ backgroundImage: `url(${contact.avatar})`}} />
          <div className="username">{contact.username}</div>
        </header>
      }
      <div className="messages">
        {messages.map(m =>
          <div className={'message ' + (m.dst === parseInt(id) ? 'own' : 'remote')} key={m.id}>
            {m.message}
          </div>
        )}
      </div>
      <form className="footer" onSubmit={handleSubmit}>
        <input placeholder="Escribe aquí..." value={message} onChange={e => setMessage(e.target.value)} />
      </form>
    </div>
  )
}

export default ChatRoom
