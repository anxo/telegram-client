import { Route, Switch } from 'react-router-dom'
import Home from './Home'
import Login from './Login'
import Signup from './Signup'

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/login" exact>
          <Login />
        </Route>
        <Route path="/signup" exact>
          <Signup />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </div>
  )
}

export default App
