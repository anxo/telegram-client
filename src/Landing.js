import { Link } from 'react-router-dom'

function Landing() {
  return (
    <div className="page page-landing">
      <main className="landing dialog">
        <h1>Bienvenido a HAB-Telegram!</h1>
        <p>Entra para chatear con tus compañeros:</p>
        <p>
          <Link to="/login" className="button">Iniciar sesión</Link>
        </p>
        <p>
          <span>Aún no tienes cuenta?</span>
          <Link to="/signup">Regístrate</Link>
        </p>
      </main>
    </div>
  )
}

export default Landing
