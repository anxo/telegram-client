import { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

function Signup() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState(null)
  const user = useSelector(s => s.user)
  const dispatch = useDispatch()

  const handleSubmit = async e => {
    e.preventDefault()
    const ret = await fetch('http://telegram-api.trek-quest.com/register', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ username, password })
    })
    const data = await ret.json()
    if (ret.ok) {
      dispatch({ type: 'user/login', user: data })
    } else {
      setError(data.error)
    }
  }

  if (user) return <Redirect to="/" />

  return (
    <div className="page page-signup">
      <main className="signup dialog">
        <h1>Registro</h1>
        <form onSubmit={handleSubmit}>
          <label>
            <span>Usuario:</span>
            <input value={username} onChange={e => setUsername(e.target.value)} required />
          </label>
          <label>
            <span>Contraseña:</span>
            <input type="password" value={password} onChange={e => setPassword(e.target.value)} required />
          </label>
          <button>Registro</button>
          {error &&
            <div className="error">{error}</div>
          }
        </form>
        <p>
          <span>Ya tienes cuenta?</span>
          <Link to="/login">Inicia sesión</Link>
        </p>
      </main>
    </div>
  )
}

export default Signup
