import { applyMiddleware, combineReducers, createStore } from 'redux'

const userReducer = (state = null, action) => {
  switch (action.type) {
    case 'user/login':
      return action.user
    case 'user/logout':
      return null
    default:
      return state
  }
}

const contactReducer = (state = {}, action) => {
  switch (action.type) {
    case 'fetch/chatlist':
      const newState = { ...state }
      action.data.forEach(c => {
        newState[c.id + ""] = { id: c.id, username: c.username, avatar: c.avatar }
      })
      return newState
    default:
      return state
  }
}

const messageReducer = (state = {}, action) => {
  let newState
  switch (action.type) {
    case 'fetch/chatlist':{
        newState = { ...state }
        action.data.forEach(c => {
          if (c.lastMessage) {
            newState[c.id + ""] = [c.lastMessage]
          }
        })
        return newState
      }
    case 'fetch/chat':
      newState = { ...state }
      newState[action.data.user.id + ""] = action.data.messages
      return newState
    case 'ws/message':
      const target = action.me === action.message.src ? action.message.dst : action.message.src
      newState = { ...state }
      newState[target + ""] = [action.message, ...(newState[target + ""] || [])]
      return newState
    default:
      return state
  }
}

const sessionStorageMiddleware = store => next => action => {
  let result = next(action)
  sessionStorage.setItem('session', JSON.stringify(store.getState()))
  return result
}

const store = createStore(
  combineReducers({
    user: userReducer,
    contacts: contactReducer,
    messages: messageReducer
  }),
  JSON.parse(sessionStorage.getItem('session')) || {},
  applyMiddleware(sessionStorageMiddleware)
)

export default store
