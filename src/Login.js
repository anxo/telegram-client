import { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

function Login() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState(null)
  const user = useSelector(s => s.user)
  const dispatch = useDispatch()

  const handleSubmit = async e => {
    e.preventDefault()
    const ret = await fetch('http://telegram-api.trek-quest.com/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ username, password })
    })
    const data = await ret.json()
    if (ret.ok) {
      dispatch({ type: 'user/login', user: data })
    } else {
      setError(data.error)
    }
  }

  if (user) return <Redirect to="/" />

  return (
    <div className="page page-login">
      <main className="login dialog">
        <h1>Iniciar sesión</h1>
        <form onSubmit={handleSubmit}>
          <label>
            <span>Usuario:</span>
            <input value={username} onChange={e => setUsername(e.target.value)} required />
          </label>
          <label>
            <span>Contraseña:</span>
            <input type="password" value={password} onChange={e => setPassword(e.target.value)} required />
          </label>
          <button>Iniciar sesión</button>
          {error &&
            <div className="error">{error}</div>
          }
        </form>
        <p>
          <span>Aún no tienes cuenta?</span>
          <Link to="/signup">Regístrate</Link>
        </p>
      </main>
    </div>
  )
}

export default Login
