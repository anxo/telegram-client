import Landing from "./Landing"
import Dashboard from "./Dashboard"
import { useSelector } from "react-redux"

function Home() {
  const user = useSelector(s => s.user)

  if (!user) return <Landing />
  return <Dashboard />
}

export default Home
