import { Link } from 'react-router-dom'
import './ChatEntry.css'

function DateViewer({ value }) {
  const now = new Date().toISOString()
  if (value.substr(0, 10) === now.substr(0, 10)) {
    return value.substr(11, 5)
  } else {
    return value.substr(8, 2) + '/' + value.substr(5, 2) + '/' + value.substr(0, 4)
  }
}

function ChatEntry({ chat }) {
  return (
    <Link className="chat-entry" to={'/chat/' + chat.id}>
      <div className="avatar" style={{ backgroundImage: `url(${chat.avatar})`}} />
      <div className="info">
        <div className="top-row">
          <div className="contact-name">{chat.username}</div>
          {chat.lastMessage &&
            <div className="date"><DateViewer value={chat.lastMessage.date} /></div>
          }
        </div>
        <div className="bottom-row">
          {chat.lastMessage &&
            <div className="message">{chat.lastMessage.message}</div>
          }
        </div>
      </div>
    </Link>
  )
}

export default ChatEntry
