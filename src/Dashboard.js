import { Switch, Route } from 'react-router-dom'
import ChatList from './ChatList'
import ChatRoom from './ChatRoom'
import './Dashboard.css'

function Dashboard() {
  return (
    <div className="page page-dashboard">
      <main className="dashboard window">
        <ChatList />
        <Switch>
          <Route path="/" exact>
            <div className="dbc welcome">
              Selecciona una conversación para continuar
            </div>
          </Route>
          <Route path="/chat/:id" exact>
            <ChatRoom />
          </Route>
        </Switch>
      </main>
    </div>
  )
}

export default Dashboard
